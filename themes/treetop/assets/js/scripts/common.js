~(function($){
    function init(){
        $('.question__btn').click(event => {
  
            let parent = $(event.target).closest('.questions__question')
            let title = parent.find('.question')
            let target = parent.find('.questions__answer')
  
              if (parent.hasClass('active')) {
                  parent.removeClass('active')
                  target.slideUp()
                  title.attr('aria-expanded', 'false')
              } else {
                  parent.addClass('active')
                  target.slideDown()
                  title.attr('aria-expanded', 'true')
              }
  
        })
    }
    $(document).ready(init)
  
  })(jQuery)
  