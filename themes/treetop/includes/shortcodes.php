<?php

//adds a class of button to a link
add_shortcode('button', function ($args, $content) {
  $color = !empty($args['color']) ? $args['color'] : 'normal';
  $style = !empty($args['style']) ? $args['style'] : 'primary';
  return str_replace('<a', '<button class="button ' . $style . '"><a ', $content);
  // return str_replace('<a ', "<a class='button button--{$color} button--{$style}' ", $content);
});
