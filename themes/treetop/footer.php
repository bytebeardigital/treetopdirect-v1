<?php
/**
 * Footer template
 *
 * @author   <Author>
 * @version  1.0.0
 * @package  <Package>
 */

$weekdays = get_field('week_days', 'options');
$weekends = get_field('weekend', 'options');
$weekday_hrs = get_field('week_day_hours', 'options');
$weekend_hrs = get_field('weekend_hours', 'options');
$footer_time = get_field('mini_hours', 'options');
$closed = get_field('closed_days', 'options');
$phone = get_field('phone_number', 'options');
?>

</main>
<footer class="footer container">
    <div class="footer__inner">
        <?php the_custom_logo(); ?>
        <div class="footer__content">
            <?php echo (!empty($footer_time)) ? '<div class="footer__hours">' . $footer_time . '</div>' : '' ;?>
            <p class="text-order">Text to Order: <?php echo (!empty($phone)) ? phone_to_sms($phone, 'text-link') : '' ;?></p>
        </div>
        <?php wp_nav_menu([
            'menu'=> 'primary',
'               theme_location' => 'primary','container' => false,
                'menu_class' => 'footer__menu',
                // 'fallback_cb' => '__return_false',
                'items_wrap' => '<ul id="%1$s" class="nav me-auto justify-content-center %2$s">%3$s</ul>',
                                'depth' => 2,
                'walker' => new bootstrap_5_wp_nav_menu_walker()
            ]);
?>
    </div>
</footer>
<?php wp_footer(); ?>
</body>
</html>
