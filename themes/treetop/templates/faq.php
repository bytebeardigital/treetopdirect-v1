<?php
/** Template Name: FAQ
 *
 *
 * @author   B. Gardner
 * @version  1.0.0
 */

get_header();
megaComponent('hero', 'page');
?>

<div class="container-lg faq">
    <!--HOW TO ORDER--->
        <?php 
            component('how-to-order');
            component('questions');
        ?>
</div>

<?php 
megaComponent('cta', 'banner');
get_footer();?>