<?php

// converts a phone number to a tel: link
function phone_to_link($num, $classes = []){
    $number_only = preg_replace('/[^0-9]/', '', $num);
    return '<a class="link ' .$classes .'" href="tel:'.$number_only.'">'.$num.'</a>';
  }

  // converts a phone number to a sms: link
function phone_to_sms($num, $classes = []){
  $number_only = preg_replace('/[^0-9]/', '', $num);
  return '<a class="link ' .$classes .'" href="sms:'.$number_only.'">'.$num.'</a>';
}

// converts email address to a mailto: link
function email_to_link($email){
    return '<a class="link" href="mailto:'. antispambot( $email ).'">'.esc_html( antispambot( $email ) ) .'</a>';
  }
  
