<?php
/*** Component functionality
 * */
function megaComponent($parent, $name, $args = [], $echo = true){
  $html = '';
 
  //set each key as a variable
  foreach($args as $key => $value){
    $$key = $value;
  }
 
  if($echo == false){
    ob_start();
  }
  //components/parent/children/parent-children.php
  //example: components/hero/home-hero.php
  //include the template to have access to variables
  $template_location = 'components/' . $parent . '/' . $name . '/' . $parent . '-' . $name . '.php';
  if(locate_template($template_location)){
    require(get_template_directory() . '/' . $template_location);
  } else {
    pr($parent . '-' . $name . ' component not found.');
  }
 
  if($echo == false){
    $html = ob_get_contents();
    ob_end_clean();
  }
 
  //unset any variables we've created
  foreach($args as $key => $value){
    unset($$key);
  }
 
  return $html;
 }

/*** Component functionality
 * */
function component($name, $args = [], $echo = true){
    $html = '';
   
    //set each key as a variable
    foreach($args as $key => $value){
      $$key = $value;
    }
   
    if($echo == false){
      ob_start();
    }
    
    //include the template to have access to variables
    $template_location = 'components/' . $name . '/' . $name . '.php';
    if(locate_template($template_location)){
      require(get_template_directory() . '/' . $template_location);
    } else {
      pr($name . ' component not found.');
    }
   
    if($echo == false){
      $html = ob_get_contents();
      ob_end_clean();
    }
   
    //unset any variables we've created
    foreach($args as $key => $value){
      unset($$key);
    }
   
    return $html;
   }


/*** Theme Support ***/
add_theme_support('post-thumbnails');
add_theme_support( 'title-tag' );
add_theme_support( 'custom-header' );
add_theme_support( 'custom-logo' );
add_theme_support('post');
add_theme_support('html5');
add_theme_support('menus');
add_theme_support('responsive-embeds');

  /**
 * Tell woocommerce that our theme can support it
 */
add_action( 'after_setup_theme', function(){
  add_theme_support( 'woocommerce' );
});