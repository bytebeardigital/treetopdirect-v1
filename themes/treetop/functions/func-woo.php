<?php
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart');

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart');

// remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_title', 5);

/** Remove product data tabs */
add_filter( 'woocommerce_enable_order_notes_field', '__return_false' );
add_filter( 'woocommerce_product_tabs', 'my_remove_product_tabs', 98 );
 
function my_remove_product_tabs( $tabs ) {
  unset( $tabs['additional_information'] ); // To remove the additional information tab
  return $tabs;
}

add_filter( 'woocommerce_is_purchasable', '__return_false' );

// Utility function to get the parent variable product IDs for a any term of a taxonomy
function get_variation_parent_ids_from_term( $term, $taxonomy, $type ){
  global $wpdb;

  return $wpdb->get_col( "
      SELECT DISTINCT p.ID
      FROM {$wpdb->prefix}posts as p
      INNER JOIN {$wpdb->prefix}posts as p2 ON p2.post_parent = p.ID
      INNER JOIN {$wpdb->prefix}term_relationships as tr ON p.ID = tr.object_id
      INNER JOIN {$wpdb->prefix}term_taxonomy as tt ON tr.term_taxonomy_id = tt.term_taxonomy_id
      INNER JOIN {$wpdb->prefix}terms as t ON tt.term_id = t.term_id
      WHERE p.post_type = 'product'
      AND p.post_status = 'publish'
      AND p2.post_status = 'publish'
      AND tt.taxonomy = '$taxonomy'
      AND t.$type = '$term'
  " );
}

add_action('template_redirect', 'remove_shop_breadcrumbs' );
function remove_shop_breadcrumbs(){
 
    if (is_shop())
        remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0);
        remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0);
 
}
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title');
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 50 );

remove_action('woocommerce_after_single_product_summary','woocommerce_output_related_products', 20);
remove_action('woocommerce_after_single_product_summary','woocommerce_output_related_products', 20);

function remove_product_editor() {
  remove_post_type_support( 'product', 'editor' );
}
add_action( 'init', 'remove_product_editor' );

/**NO PRODUCTS */


	/**
	 * Handles the loop when no products were found/no product exist.
	 */
	function give_us_a_call() {
		get_template_part( 'loop' , 'no-product-call-us' );
	}

  function get_variations(){
    global $woocommerce, $product, $post;
    if( $product->is_type( 'variable' ) ){
          // Loop through available product variation data
          foreach ( $product->get_available_variations() as $key => $variation ) {
              // Loop through the product attributes for this variation
              foreach ($variation['attributes'] as $attribute) {
                echo '<p>' .$attribute. '</p>';
              }
            }
          } 
  }



  function find_valid_variations() {
    global $product;
 
    $variations = $product->get_available_variations();
    $attributes = $product->get_attributes();
    $new_variants = array();
 
    // Loop through all variations
    foreach( $variations as $variation ) {
 
        // Peruse the attributes.
 
        // 1. If both are explicitly set, this is a valid variation
        // 2. If one is not set, that means any, and we must 'create' the rest.
 
        $valid = true; // so far
        foreach( $attributes as $slug => $args ) {
            if( array_key_exists("attribute_$slug", $variation['attributes']) && !empty($variation['attributes']["attribute_$slug"]) ) {
                // Exists
 
            } else {
                // Not exists, create
                $valid = false; // it contains 'anys'
                // loop through all options for the 'ANY' attribute, and add each
                foreach( explode( '|', $attributes[$slug]['value']) as $attribute ) {
                    $attribute = trim( $attribute );
                    $new_variant = $variation;
                    $new_variant['attributes']["attribute_$slug"] = $attribute;
                    $new_variants[] = $new_variant;
                }
 
            }
        }
 
        // This contains ALL set attributes, and is itself a 'valid' variation.
        if( $valid )
            $new_variants[] = $variation;
 
    }
 
    return $new_variants;
}


?>

