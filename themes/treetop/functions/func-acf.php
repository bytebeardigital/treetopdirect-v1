<?php
/**
 * Advancded Custom Fields
 * 
 * 
 */

if( function_exists('acf_add_options_page') ) {
    acf_add_options_page(array(
		'page_title' 	=> 'Theme Options',
		'menu_title'	=> 'Theme Options',
		'menu_slug' 	=> 'theme-options',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
  acf_add_options_sub_page(array(
		'page_title' 	=> 'Home Page Settings',
		'menu_title'	=> 'Home Page Settings',
		'parent_slug'	=> 'theme-options',
	));
  acf_add_options_sub_page(array(
		'page_title' 	=> 'Page Settings',
		'menu_title'	=> 'Page Settings',
		'parent_slug'	=> 'theme-options',
	));
    acf_add_options_sub_page(array(
		'page_title' 	=> 'Shop Settings',
		'menu_title'	=> 'Shop Settings',
		'parent_slug'	=> 'theme-options',
	));

}

/**
* converts ACF link array to pretty 
*/
function acf_link($link, $classes='') {
    if(empty($link['target'])){
      $target = '_self';
    }
    else{
      $target = $link['target'];
    }
    if(!empty($link['title'])){
        $title = $link['title'];
    }
    else{
      $title = $link['title'];
    }
    return '<a class="'.$classes.'" target="'.$target.'" href="'.$link['url'].'">'.$title.'</a>';
  }