<?php

$steps = get_field('steps', 'options');
$questions = get_field('questions', 'options');
?>


<section class="faq__how-to-order">
        <h2 class="page-heading">How to Order</h2>
        <ol class="faq__order-steps">
    <?php foreach($steps as $step) :?>
        <li class="step">
            <?php echo $step['step']; ?>
            <?php echo (!empty($step['details'])) ? '<ul class="details"><li>' . $step['details'] . '</li></ul>' : ''  ; ?>
        </li>
        <?php endforeach; ?>
</ol>
    </section>