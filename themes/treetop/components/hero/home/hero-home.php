<?php
$hero_bkg = get_field('background_type', 'options');
$hero_title = get_field('hero_title', 'options');
$hero_image = get_field('background_image', 'options');
$hero_type = get_field('hero_type', 'options');
$links = get_field('hero_buttons', 'options');
$hero_theme = get_field('hero_theme', 'options');
$content = get_field('hero_content', 'options');


if($hero_theme === 'light'){
    $button_style = "btn__primary";
} else  $button_style = "btn__alt";

?>

<div class="container-lg hero hero__<?php echo $hero_type?> hero__style--<?php echo $hero_bkg; ?> <?php echo $hero_theme?>" <?php echo ($hero_bkg === "image") ? 'style="background-image:
    linear-gradient(120deg, rgba(58, 28, 113, 0.60), rgba(215, 109, 119, 0.60), rgba(255, 175, 123, 0.60)),
    url(' . $hero_image .');"' : '' ;?>">
    <div class="hero__inner row">
        <div class="hero__content">
            <h1 class="hero__content--title"><?php echo $hero_title; ?></h1>
            <?php if($hero_type === 'home'); ?>
            <div class="hero__content--button-group">
                <?php foreach($links as $link) :?>
                    <?php echo !empty($link['link']) ? acf_link($link['link'], implode(['cta-btn btn ', $button_style])) : ''; ?>
                    <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>