<?php
// PRODUCT HERO
?>

<div class="hero container-lg">
    <div class="hero__inner">
        <div class="hero__content">
            <h1 class="hero__content--title">PRODUCT TITLE</h1>
            <div class="hero__content--breadcrumbs">
            </div>
        </div>
    </div>
</div>