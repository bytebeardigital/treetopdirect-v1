<?php
$hero_bkg = get_field('background_type');
$hero_title = get_field('hero_title');
$hero_image = get_field('background_image');
$hero_type = get_field('hero_type');
$links = get_field('hero_buttons');
$hero_theme = get_field('hero_theme');
$content = get_field('hero_content');


if($hero_theme === 'light'){
    $button_style = "btn__primary";
} else  $button_style = "btn__alt";

if(!empty($hero_type)){
    $hero_type;
}
if(!empty($hero_theme)){
    $hero_theme;
}
if(!empty($hero_bkg)){
    $hero_bkg;
}
?>
<div class="container-lg hero hero__<?php echo (!empty($hero_type)) ? $hero_type : '' ;?> hero__style--<?php echo (!empty($hero_bkg)) ? $hero_bkg : '' ;?> <?php echo (!empty($hero_theme)) ? $hero_theme : '' ;?>" <?php echo ($hero_bkg === "image") ? 'style="background-image:
    linear-gradient(120deg, rgba(58, 28, 113, 0.60), rgba(215, 109, 119, 0.60), rgba(255, 175, 123, 0.60)),
    url(' . $hero_image .');"' : '' ;?>">
    <div class="hero__inner row">
        <div class="hero__content">
            <h1 class="hero__content--title"><?php the_title(); ?></h1>
            <?php echo (!empty($content)) ? '<div class="hero__content--content">' .  $content  .'</div>' : '' ;?>
            <?php if(is_front_page()): ?>
            <div class="hero__content--button-group">
                <?php foreach($links as $link) :?>
                    <?php echo !empty($link['link']) ? acf_link($link['link'], implode(['cta-btn btn ', $button_style])) : ''; ?>
                    <?php endforeach; ?>
            </div>
            <?php else: ?>
            <?php endif;?>
        </div>
    </div>
</div>