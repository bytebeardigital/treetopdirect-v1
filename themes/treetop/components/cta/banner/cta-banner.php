<?php
$title = get_field('banner_title', 'options');
$content = get_field('banner_content', 'options');
$banner_link = get_field('banner_link', 'options');
$background = get_field('banner_background', 'options');
?>

<div class="cta__banner container-lg" <?php echo (!empty($background)) ? 'style="background: linear-gradient(90deg, rgba(58, 28, 113, 0.6),rgba(215, 109, 119, 0.6), rgba(255, 175, 123, 0.6)), url('. $background .') no-repeat; background-size: cover; background-position: center"' : 'style="background-color:#193d6a;"' ;?>
    <div class="row">
        <div class="cta__banner--content">
        <?php echo !empty($title) ? '<h2 class="heading">'. $title . '</h2>' : '' ?>
            <?php echo !empty($content) ? $content : '' ?>
        <?php echo !empty($banner_link) ? acf_link($banner_link, 'btn btn__special') : '' ?>
        </div>
    </div>
</div>