<?php

$deals = get_field('deals', 'options');
$order_bkg = get_field('order_background', 'options');
$order_steps = get_field('order_steps', 'options');
$button = get_field('order_button', 'options');

?>
<div class="cta__block container-lg">
    <div class="row cta__block--inner">
        <div class="col-lg-6 col-sm-12 cta__block--left">
            <h3 class="heading">Deals</h3>
            <div class="cta__block--deals">
                <?php if(!empty($deals)) :?>
                    <?php foreach($deals as $deal) :?>
                        <?php echo $deal['content']?>
                    <?php endforeach; ?>
                <?php endif;?>
            </div>
        </div>
        <div class="col-lg 6 col-sm-12 cta__block--right" <?php echo (!empty($order_bkg )) ? 'style="background: linear-gradient(90deg,rgba(13, 31, 53, 0.5), rgba(13, 31, 53, 0.75) ), url('. $order_bkg  .') no-repeat; background-size: cover;"' : 'style="background-color:#193d6a;"' ;?>>
            <div class="cta__block--content">
            <h3 class="heading">How to Order</h3>
            <ol class="cta__block--orders">
                <?php if(!empty($order_steps)) :?>
                    <?php foreach($order_steps as $order_step):?>
                        <li class="step"><?php echo $order_step['single_step'];?></li>
                    <?php endforeach; ?>
                <?php endif; ?>
            </ol>
            <?php echo !empty($button) ? acf_link($button, 'btn btn__primary') : ''; ?>

            </div>
        </div>
    </div>
</div>