<?php

$info_background = get_field('info_background', 'options');
$weekdays = get_field('week_days', 'options');
$weekends = get_field('weekend', 'options');
$weekday_hrs = get_field('week_day_hours', 'options');
$weekend_hrs = get_field('weekend_hours', 'options');
$closed = get_field('closed_days', 'options');
$aboutus = get_field('about_us', 'options');
$delivery_info = get_field('delivery_info', 'options');
$phone_number = get_field('phone_number', 'options');



?>
<div class="container-lg cta cta__info" >
    <div class="row cta__info__inner">
        <div class="col-lg-6 col-sm-12 cta__info__left text-center" <?php echo (!empty($info_background)) ? 'style="background: url('. $info_background .') no-repeat; background-size: cover;"' : 'style="background-color:#193d6a;"' ;?>>
            <div class="cta__info__left--content">
                <?php echo (!empty($delivery_info)) ? $delivery_info : '' ;?>
                <div class="cta__info__left--hours">
                <i class="far fa-clock"></i>
                    <p class="hours"><?php echo $weekdays . ' | ' . $weekday_hrs . '</br>' . $weekends . ' | ' . $weekend_hrs?></p>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-12 cta__info__right">
        <div class="cta__info__right--content">
            <?php echo (!empty($aboutus)) ? $aboutus : ''; ?>

        </div>
    </div>
</div>
</div>