<?php
$args = array(
     'taxonomy' => 'product_cat',
     'orderby' => 'name',
     'order' => 'ASC',
     'hide_empty' => true
);

?>
<div class="catmenu container-lg">
    <div class="row catmenu__outer">
    <?php foreach( get_categories( $args ) as $category ) : ?>
    <?php $cat_link = get_term_link($category->slug, $category->taxonomy); 
    $cat_img_id = get_term_meta($category->term_id, 'thumbnail_id', true);
    $cat_img = wp_get_attachment_url($cat_img_id);?>

    <div class="col-6 col-lg-3 catmenu__wrapper">
        <a href="#" class="d-flex catmenu__card" style="background-image: url(<?php echo $cat_img ?>); background-size: cover; background-position: center center;">
            <p class="catmenu__card--inner"><?php echo $category->name; ?></p>
        </a>
    </div>
    <?php endforeach;?>
    </div>

</div>