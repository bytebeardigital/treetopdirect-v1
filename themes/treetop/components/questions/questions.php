<?php

$questions = get_field('questions', 'options');
?>

<section class="questions">
    <h2 class="page-heading">Fequently Asked Questions</h2>
    <div class="questions__container">
        <?php foreach($questions as $question) :?>
            <div class="questions__question">
                <div class="questions__question--wrapper">
                <div class="question"><?php echo $question['question']; ?></div>
                <div class="question__btn" aria-expanded="false"><?php echo render_svg('down-arrow'); ?></div>

                </div>
                <div style="display: none;" class="questions__answer">
                <?php echo $question['answer']; ?>
            </div>
            </div>
        <?php endforeach; ?>
    </div>
</section>