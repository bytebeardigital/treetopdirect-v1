<header class="header navbar navbar-expand-sm">
    <nav class="container-xl header__nav">
        <div class="header__logo me-md-auto">
            <?php if(!empty(the_custom_logo())) the_custom_logo() ?>
            </div>
        </div>
        <button class="header__nav-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
            <span class="header__nav-toggler--wrapper"><i class="fas fa-bars" style="color:#193d6a;"></i></span>
        </button>
        <div class="collapse navbar-collapse" id="main-menu">
            <?php
            wp_nav_menu(array(
                'theme_location' => 'primary',
                'container' => false,
                'menu_class' => '',
                'fallback_cb' => '__return_false',
                'items_wrap' => '<ul id="%1$s" class="navbar-nav ml-auto mb-2 mb-md-0 %2$s">%3$s</ul>',
                'depth' => 2,
                'walker' => new bootstrap_5_wp_nav_menu_walker()
            ));
            ?>
        </div>
    </nav>


</header>