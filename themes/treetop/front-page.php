<?php
/**
 *
 */

defined( 'ABSPATH' ) || exit;

get_header();
megaComponent('hero', 'home');
megaComponent('cta', 'info');
component('shop-cat-menu');
megaComponent('cta', 'block');
megaComponent('cta', 'banner');
get_footer();