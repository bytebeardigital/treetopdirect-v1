<?php
/**
 * 404 template
 *
 * @author   <Author>
 * @version  1.0.0
 * @package  <Package>
 */
$info_background = get_field('info_background');
$weekdays = get_field('mini_weekdays', 'options');
$weekends = get_field('mini_weekend', 'options');
$weekday_hrs = get_field('mini_weekday_hours', 'options');
$weekend_hrs = get_field('mini_weekend_hours', 'options');
$closed = get_field('mini_closed_days', 'options');
get_header();
?>
	<article>
		<header>
			<h1><?php echo '404. Page Not Found.'; ?></h1>
		</header>
	</article>
<?php
get_footer();
